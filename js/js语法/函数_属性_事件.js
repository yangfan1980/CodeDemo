// 变量与属性的区别
var a = 123;
var object = {};//对象
//var object = document.getElementById('box');
object.name = "box";
object.name = 888888;
console.log(object.name);

//函数
function fn(){return this;};
object.fn = function(obj){return this;};
console.log(object.fn(object));

//事件
//onclick 单击
//onmouseover 鼠标滑入事件(会冒泡)
//onmouseout 鼠标滑出世间(会冒泡)
//onmouseenter 鼠标滑入事件(不会冒泡)
//onmouseleave 鼠标滑出事件(不会冒泡)
//ondbclick 双击事件

//this
console.log(fn(a)); // undefined

var foo = "123" + 4 - "1";
console.log(foo);
foo += 3;
console.log(foo);

var i=0,j=100,k=100;
function log(n){var j=n;console.log(j)};
for (i ; i<10 ; i++){var k=i;log(k);};
console.log(j);
console.log(k); 

function tmplateDataMapping(temp,data){
	if(temp){
		var matches = temp.match(/{{[a-z_A-Z](\w+)?([\.][a-z_A-Z](\w+)?)*}}/g);
		matches.each(function(index,match){
			temp = temp.replace(match,getMatchedData(data,match.substring(2,match.length-2)));
		});
	}
	return temp;
}

function getMatchedData(data,match){
	var result = data;
	var attrs = match;
	attrs.each(function(obj){
		result = typeof result == "object" && result.hasOwnProperty(obj)? result :  "";
	});
	result = typeof result == "undefined" ? "" : result;
	return result;
}

//排序:插入算法
~function Sort() {
    var arr = [6, 5, 9, 16, 45, 35, 8, 13, 25, 60, 22, 31];
    var i, j, tmp;
    for (i = 0; i < arr.length; i++) {
        tmp = arr[i];
        j = i - 1;
        while (j >= 0 && tmp < arr[j]) {
            arr[j + 1] = arr[j];
            j--;
            console.log("2:" + arr);
        }
        arr[j + 1] = tmp;
        console.log(tmp);
        console.log("1:" + arr);
    }
}();