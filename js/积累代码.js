//继承并复写js的方法;在后期添加函数时采用这种方式;这样的话,可以防止变量污染
Function.prototype.addMethod = function (name, fn) {
    this[name] = fn;
    //方便使用链式方法
    return this;
}
//Demo
var methods = function(){};
methods.addMethod('checkName',function(){
    //逻辑
    console.log(0);
    return this;
}).addMethod('checkEmail',function(){
    //逻辑
    console.log(1);
    return this;
});
//随机数
methods.addMethod('RandomNum', function () {
    var num = "";
    for (var i = 0; i < 8; i++) {
        num += Math.floor(Math.random() * 10);
    }
    return num;
})
console.log(methods.RandomNum());
//methods.checkName().checkEmail();

//安全模式创建的工厂类
var Factory = function(type,content){
    //instanceof:实例this在不在Factory函数中
    if(this instanceof Factory){
        var s = new this[type](content);
        return s;
    }else{
        return new Factory(type,content);
    }
}

console.log((new Date()).valueOf());

//在console.assert()语句中，第一个参数为需要进行assert的结果，正常情况下应当为true；第二个参数则为出错时在控制台上打印的错误信息。比如，当上述例子中score变量的数组长度不为3时：
function cat(name, age, score){
    this.name = name;
    this.age = age;
    this.score = score;
}
var c = new cat("miao", 2, [6,8,7]);
console.assert(c.score.length==3, "Assertion of score length failed");

//在js调用函数时,会默认传入两个参数:
//1.arguments:所有参数的一个集合.拥有length的属性,并且可以使用arguments[1](表示第二个参数)获取参数.但注意它不是数组形式.
//2.this

//null undefined ""的区别
var a = null;//a被初始化为一个空对象null；如果定义的变量准备在将来用于保存对象，一般将变量初始化为null
typeof a;//object

var b;//b未被初始化
typeof b;//undefined

var c = "";//c被初始化为字符串，值为空
typeof c;//string

//判断参数是否为空
function SortArray(arr){
    //如果参数是数组则运用comparator进行排序
    arr instanceof Array && arr.sort(comparator);
    Array.isArray(arr) && arr.sort(comparator);//推荐
}