public int UpdateOrderAplipayStatus(string BossID, int customerId, string OrderNum, string TransactionID, string Account, string payInfo, out int isAutoApply)
{
    isAutoApply = 0;//执行结果有无数据标识
    using (MySqlConnection conn = V_DAL.SqlHelper.Settings.GetSOPMySqlConnection(false))//建立数据库连接
    {
        try
        {
            MySqlCommand cmd = new MySqlCommand("P_SetAMPSaleOrderAlipayStatus", conn);//选择执行存储过程名称
            cmd.CommandType = CommandType.StoredProcedure;//执行命令类型为存储过程
            cmd.Parameters.Add("@BossID_Para", MySqlDbType.VarChar, 20).Value = BossID;//存储过程参数赋值
            cmd.Parameters.Add("@CustomerId_Para", MySqlDbType.Int32).Value = customerId;//存储过程参数赋值
            cmd.Parameters.Add("@OrderNum_Para", MySqlDbType.VarChar, 55).Value = OrderNum;//存储过程参数赋值
            cmd.Parameters.Add("@TransactionID_Para", MySqlDbType.VarChar, 50).Value = TransactionID;//存储过程参数赋值
            cmd.Parameters.Add("@Buyer_Para", MySqlDbType.VarChar, 50).Value = Account;//存储过程参数赋值
            cmd.Parameters.Add("@PayInfo_Para", MySqlDbType.VarChar, 2500).Value = payInfo;//存储过程参数赋值
            cmd.Parameters.Add("@IsSuccess_Para", MySqlDbType.Int32).Value = 0;//存储过程参数赋值
            cmd.Parameters["@IsSuccess_Para"].Direction = ParameterDirection.Output;//声明参数为返回值参数
            cmd.Parameters.Add("@IsAutoApply_Para", MySqlDbType.Int32).Value = 0;
            cmd.Parameters["@IsAutoApply_Para"].Direction = ParameterDirection.Output;//声明参数为返回值参数
            if (conn.State == ConnectionState.Closed)//如果数据库连接关闭则开启连接
            {
                conn.Open();
            }

            cmd.ExecuteNonQuery();//执行存储过程
            Object ObjRtID = cmd.Parameters["@IsSuccess_Para"].Value;//获取返回值参数
            int rt = ObjRtID == null || ObjRtID.ToString() == string.Empty ? -5 : (int)ObjRtID;
            Object ObjRtID_auto = cmd.Parameters["@IsAutoApply_Para"].Value;//获取返回值参数
            isAutoApply = ObjRtID_auto == null || ObjRtID_auto.ToString() == string.Empty ? -5 : (int)ObjRtID_auto;
            return rt;//返回结果
        }
        catch (Exception ex)//发生异常记录日志并返回错误代码
        {
            ActCommon.Log.WriteLog("V_DAL.AMP_SaleOrdersDAL.UpdateOrderAplipayStatus:date(" + System.DateTime.Now.ToString() + "),error:" + ex.Message.ToString(), "weixin");
            return -10000;
        }
    }
}