	-- 插入退货出库信息中不存在的信息
		SET @Sql_Para=CONCAT('INSERT INTO CosmeticReporting.CR_InventoryFlowReporting_',TabExCode3_Para,'(
			BossID,InCounterID,OutCounterID,ProductID,InventoryIn,InventoryOut,OutRecordTime,InventoryInCostPrice,InventoryOutCostPrice,
			InventoryInForDay,InventoryOutForDay,InventoryInForDayCostPrice,InventoryOutForDayCostPrice,InventoryInPrice,InventoryOutPrice,
			InventoryInProductSalePrice,InventoryOutProductSalePrice,RecordTime,AddTime,InventoryAllInForDay,InventoryAllInForDayCostPrice,
			InventoryReturnInForDay,InventoryReturnInForDayCostPrice,InventoryReturnInPrice,InventoryAllOutForDay,InventoryAllOutForDayCostPrice,
			InventoryAllOutPrice)
		SELECT ''',BossID_Para,''',OutStoreID,InStoreID,ProductID,0,0,''1900-01-01'',0,0,0,0,0,0,InventoryInPrice,0,0,0,RecordTime,NOW(),0,0,InventoryReturnInForDay,InventoryReturnInForDayCostPrice,InventoryReturnInPrice,0,0,0 FROM Tmp_InventoryReturnIn WHERE NOT EXISTS (SELECT NULL FROM CosmeticReporting.CR_InventoryFlowReporting_',TabExCode3_Para,' a WHERE a.InCounterID=Tmp_InventoryReturnIn.OutStoreID AND a.OutCounterID=Tmp_InventoryReturnIn.InStoreID AND a.ProductID=Tmp_InventoryReturnIn.ProductID AND a.RecordTime=Tmp_InventoryReturnIn.RecordTime)');