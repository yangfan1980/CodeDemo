-- 登陆用户
SELECT * FROM CR_VIPUser WHERE AddTime >= '2018-05-04'
SELECT * FROM CR_VIPUser WHERE UserName in('mc800815','sy800831',800785,800290);
SELECT * FROM CR_VIPUser WHERE StoreName LIKE '%孝感%';
SELECT * FROM CR_VIPUser WHERE VipUserID in(900059,900648,900726,900590,900328); 
SELECT * FROM CR_Staff_4 WHERE OperatorID LIKE '900003%' LIMIT 100; -- 员工表
SELECT MD5(900795);

-- 消息通知发送表 (APP推送)
SELECT * FROM CR_MessageNoticeSend WHERE AddTime >= '2018-05-04' and BossID = 900003;
SELECT * FROM CR_BAPushNoticeSend WHERE BossID = 900003 and AddTime > '2017-08-14 19:00:00';
SELECT * FROM CR_SmsPush WHERE AddTime >'2017-10-13 08:00:00';
SELECT * FROM CR_AppLoginMacRegister a INNER JOIN CR_Staff_4 b on a.StaffID=b.StaffID WHERE a.StoreID=2399 AND b.Category=20;
SELECT * FROM CR_AppLoginMacRegister WHERE StoreID=5091;
SELECT * FROM CR_ArcSoftApp;
select * from CR_ArcSoftAppKey;
SELECT * FROM CR_ArcSoftCloudApp;
SELECT * FROM CR_Staff_4 WHERE OperatorID LIKE '900003%';
SELECT * FROM CR_VIPUser WHERE UserType = 10;
SELECT a.ID,a.AppID,a.StoreID,a.StoreName,a.StaffID,a.StaffCode,a.StaffName,a.MacAddress,a.AddTime FROM CR_AppLoginMacRegister a 
INNER JOIN CR_Staff_4 b on a.StaffID=b.StaffID WHERE a.StoreID=5091 AND b.Category=20 AND b.Status > 0 And b.OperatorID LIKE '900003%';

-- 用户微商城模板
SELECT * FROM CR_VIPUserTemplate WHERE BossID in(900340,900476);
SELECT * FROM CR_VIPUserTemplate ORDER BY bossid;

-- 门店表
SELECT * FROM CR_Store WHERE OperatorID LIKE '900264%' AND Category = 20 and status > 0 AND Name like '%欧%';

-- 库存相关
select * from CR_Inventory_10 WHERE ProductCNName LIKE '%丸美纯色之恋晶莹剔透鲜养面膜25G/片*6%';
SELECT * from CR_Inventory_42 where OperatorID LIKE '900003%';
SELECT * FROM CR_InventoryLog_960 WHERE AddTime >= '2018-01-01' and ProductID in (6961) and StoreID = 9618 limit 100;
SELECT * FROM CR_InventoryAvgCostPriceLog_960 WHERE AddTime >='2018-04-17' and ProductID = 8952 LIMIT 100; -- 库存成本调整单

-- 会员相关
SELECT * FROM CR_Consumer_107 where OperatorID LIKE '900297%' and Mobile = '13806600718' and `Status` > 0;
SELECT * FROM CR_Consumer_a0d WHERE MemberCode in(217293,245946);
SELECT * FROM CR_Consumer_7e0 WHERE Birthday < '1950-01-01';
SELECT * FROM CR_Consumer_559 WHERE CardCode = 15771763942;

-- 商品相关
SELECT * FROM CR_Product_ff WHERE  BarCode in('6930331833364','6902395329411','6902395308263') LIMIT 100;
SELECT * FROM CR_Product_96 WHERE ProductID=8146 and TypeID = 18 and OperatorID like '900690%';
SELECT * FROM CR_Product_f5 WHERE  TypeID = 31 and OperatorID like '900690%';
SELECT * from CR_SalePriceAdjustLog_9 WHERE AddTime >= '2018-04-17' and OperatorID LIKE '900795%' and BarCode = '3579200365945' LIMIT 10; -- 销售价格调整单
SELECT * FROM CR_ArcsoftProduct;-- 云管理产品库

-- 库存单据
select * from CR_SaleBill_103 WHERE AddTime >= '2018-04-18' AND StoreID = 5938 and Category = 20;
select * from CR_SaleBill_6e9 WHERE AddTime >= '2018-05-03' AND StoreID = 5938 and Category = 10;
SELECT * FROM CR_BrandAddInventoryBill_9 WHERE AddTime >= '2018-04-09' and OperatorID like '900795%' and ID in(110988,112705); -- 入库单
SELECT * FROM CR_BrandAddInventoryLog_96 WHERE AddTime >= '2018-04-09' and OperatorID like '900795%' and ProductID = 8952 and StoreID = 9622; -- 入库明细单

-- 促销活动
SELECT * FROM CR_PromotionActivitySet_4 WHERE AddTime > '2018-01-01' AND ActivityType = 110 LIMIT 100; -- 促销活动设置单

-- 报表
select * FROM CR_SSLinkDaySumData_422 WHERE AddTime > '2018-05-08'; -- 单据数据(零售通)

-- 其他
SELECT * FROM CR_Area LIMIT 100; -- 地区
SELECT * FROM CR_BrandPermissions WHERE PermissionID =3605 AND OperatorID=900009; -- 权限
SELECT * from CR_SystemSet WHERE SetType=215 AND OperatorID LIKE '900704%'; -- 系统设置(打印微信公众平台二维码)
SELECT * FROM CR_ArcsoftLeftNavigation WHERE status > 0 and url <> '';-- 左边导航

