-- 每天运行的服务及日志
SET @date = '2018-04-19';
SELECT * from CR_TaskSchedulePlan;
SELECT * from CR_TaskSchedulePlanLog WHERE AddTime >= @date and Status = '-5';
-- 查询当天任务执行情况
SELECT * from CR_TaskSchedulePlanLog WHERE AddTime >= @date ORDER BY AddTime DESC;

-- 根据报表关联的表查询有关联的存储过程
SELECT * from CR_TaskSchedulePlan WHERE ProcName LIKE '%MemberMonthlyReporting%';
SELECT * from CR_TaskSchedulePlan WHERE ProcName LIKE '%Reporting%';

-- 查询部分任务
SELECT * from CR_TaskSchedulePlan WHERE TaskID in (21,30);


-- 过渡语句
SELECT * from CR_VIPUser WHERE UserName ='800767';
SELECT MD5('900777'); -- 195741bb790ddc974d93b731c1dfb76cs
SELECT * from CR_Staff_1 WHERE StaffCode='800767';
SELECT * FROM CR_Product_19 WHERE BarCode ='6930957821974';

-- 根据BossID及日起执行库存相关报表
CALL CRP_GetReportingInventoryByBossID('900109','2018-04-01');

-- 按日期执行库存相关报表 PS:需先改存储过程中的日期
CALL P_Schedule_GetReportingInventoryTest();

-- 零售管理助手昨日数据没有时,运行的存储过程 PS:可以调节前三天到今天的数据
CALL P_Schedule_GetReportingCounter();
-- Ps:此过程是按BossID,起止日期来调整的
call CRP_GetReportingCounterByBossID('900476','2018-04-19','2018-04-20');

-- 产品分析销售报表数据没有时,运行的存储过程 PS:可以调节前三天到今天的数据
call P_Schedule_GetReportingBAProduct();



-- 其他
CALL CRP_GetReportingBAProductByBossID('900476','2018-04-19','2018-04-20');

SELECT * FROM CR_Inventory_42 WHERE StoreID=5091 AND `Status` < 0 AND BarCode='6955818236766';
SELECT * from CR_Store WHERE `Name`='abc良渚1号店';
SELECT * from CR_Store WHERE OperatorID like '900777%';

SELECT MONTH(NOW());

SELECT Category from CR_ConsumeLog_42 WHERE OperatorID LIKE '900003' GROUP BY Category;

SELECT * FROM P_GetSaleInfoListForBossApp LIMIT 100;

CALL P_Schedule_GetReportingCounter();


